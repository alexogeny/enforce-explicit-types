# enforce-explicit-types

An eslint plugin to enforce explicit types in TypeScript projects.